program MazeRecursive;
uses crt;
const w = '#'; s = ' ';
      SAVEFILE = 'RECURSIVEMAZE.txt';
type mas = array [1..100,1..100] of char;
     bool = array [1..8] of boolean;

var H, L: integer;

procedure saveMaze(M: mas);
var i, j: integer; D: text;
begin
  assign(D, SAVEFILE); rewrite(D);
  for i := 1 to L do
  begin
    for j := 1 to H do
      write(D, M[i,j]);
    writeln(D);
  end;
  close(D);
end;

procedure initMaze(var M: mas);
var i, j: integer;
begin
  for i := 1 to L do
    for j := 1 to H do
      M[i,j] := w;
  M[2,2] := s;
end;

procedure drawMaze(M: mas);
var i, j: integer;
begin
  for i := 1 to L do
  begin
    for j := 1 to H do
      if M[i,j] = w tHen write(#219) else write(s);
    writeln;
  end;
end;

procedure checkAround(var t: bool; M: mas; y, x: integer);
begin
  t[2] := M[y-1,x] = s;
  t[4] := M[y,x+1] = s; t[6] := M[y+1,x] = s;
  t[8] := M[y,x-1] = s;
  t[1] := M[y-1,x-1] = s; t[3] := M[y-1,x+1] = s;
  t[5] := M[y+1,x+1] = s; t[7] := M[y+1,x-1] = s;
end;

function checkEdges(M: mas; y, x: integer): boolean;
var t: bool; t1: bool;
begin
  checkEdges := false;
  checkAround(t,M,y,x);
  randomize;
  if (x <= H-1) and (y <= L-1) then
  if (M[y,x] <> s) then
  begin
      t1[2] := t[2] and not t[4] and not t[6] and not t[8] and not t[5] and not t[7];
      t1[4] := t[4] and not t[2] and not t[8] and not t[6] and not t[1] and not t[7];
      t1[6] := t[6] and not t[2] and not t[4] and not t[8] and not t[1] and not t[3];
      t1[8] := t[8] and not t[2] and not t[4] and not t[6] and not t[5] and not t[3];

      if t1[2] or t1[4] or t1[6] or t1[8] then
      checkEdges := true;
  end;
end;

{function checkEnd(M: mas): boolean;
var i, j: integer;
begin
  checkEnd := true;
  for i := 2 to (L-1) do
    for j := 2 to (H-1) do
      if checkEdges(M,i,j) = true then
      checkEnd := false;
end; }

procedure makeMaze(y, x: integer; var M: mas);
var r: integer;
begin
  gotoxy(1,4); drawMaze(M);
  if checkEdges(M, y, x) and ((x <> 2) or (y <> 2)) then
  begin
    M[y,x] := s; randomize; r := random(4);
    case r of
        0: begin
             if (x+1<H) then makeMaze(y,x+1, M);
             if (y+1<L) then makeMaze(y+1,x, M);
             if (x-1>1) then makeMaze(y,x-1, M);
             if (y-1>1) then makeMaze(y-1,x, M);
           end;
        1: begin
             if (x-1>1) then makeMaze(y,x-1, M);
             if (y-1>1) then makeMaze(y-1,x, M);
             if (y+1<L) then makeMaze(y+1,x, M);
             if (x+1<H) then makeMaze(y,x+1, M);
           end;
        2: begin
             if (x-1>1) then makeMaze(y,x-1, M);
             if (y+1<L) then makeMaze(y+1,x, M);
             if (x+1<H) then makeMaze(y,x+1, M);
             if (y-1>1) then makeMaze(y-1,x, M);
           end;
        3: begin
             if (x+1<H) then makeMaze(y,x+1, M);
             if (x-1>1) then makeMaze(y,x-1, M);
             if (y-1>1) then makeMaze(y-1,x, M);
             if (y+1<L) then makeMaze(y+1,x, M);
           end;
        end;
  end
  else saveMaze(M);
end;

var Maze: mas; c: char;
begin
  cursoroff;
  write('H & L: '); read(H, L);
  initMaze(Maze);
  makeMaze(2,3,Maze);
  writeln('Maze compleat');
  Readln; Readln;
end.
