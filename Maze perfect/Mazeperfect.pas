program Maze_perfect;
uses crt;
const MAXSIZE = 100;
      SAVEFILE = 'perfect.txt';
      s = '#';

type mas = array [1..MAXSIZE,1..MAXSIZE] of char;
     bool = array [1..8] of boolean;

var H, L: integer;  //Hight and lenght

procedure drawMaze(M: mas);
var i, j: integer;
begin
  for i := 1 to L do
  begin
    for j := 1 to H do
      write(M[i,j]);
    writeln;
  end;
end;

procedure getWalls(var M: mas);
var i, j: integer;
begin
  for i := 1 to L do
  begin
    for j := 1 to H do
    begin
      if (i = 1) or (i = L) then M[i,j] := s else
      if (j = 1) or (j = H) then M[i,j] := s;
      {if (i = 1) and (j mod 3 = 0) then M[i+1, j] := s;
      if (i = L) and (j mod 3 = 0) then M[i-1, j] := s;
      if (j = 1) and (i mod 3 = 0) then M[i, j+1] := s;
      if (j = H) and (i mod 3 = 0) then M[i, j-1] := s;}
    end;
  end;
  //M[L div 2, H div 2] := s;
end;

procedure saveMaze(M: mas);
var i, j: integer; D: text;
begin
  assign(D, SAVEFILE); rewrite(D);
  for i := 1 to L do
  begin
    for j := 1 to H do
      write(D, M[i,j]);
    writeln(D);
  end;
  close(D);
end;

procedure checkAround(var t: bool; M: mas; y, x: integer);
begin
  t[1] := M[y-1,x-1] = s; t[2] := M[y-1,x] = s; t[3] := M[y-1,x+1] = s;
  t[4] := M[y,x+1] = s; t[5] := M[y+1,x+1] = s; t[6] := M[y+1,x] = s;
  t[7] := M[y+1,x-1] = s; t[8] := M[y,x-1] = s;
end;

function checkEdges(M: mas; y, x: integer): boolean;
var t: bool; t1: bool;  i, j: integer; t2, t3: boolean;
begin
  checkEdges := false;
  checkAround(t,M,y,x);
  randomize;
  if (x <= H-1) and (y <= L-1) then
  if (M[y,x] <> s) then
  begin
    if (x = 2) or (y = 2) or (x = H-1) or (y = L-1) then
    begin
      if (x = 2) then
      if not t[2] and not t[3] and not t[4] and not t[5] and not t[6] then
      checkEdges := true;
      if (y = 2) then
      if not t[8] and not t[7] and not t[4] and not t[5] and not t[6] then
      checkEdges := true;
      if (x = H-1) then
      if not t[2] and not t[1] and not t[8] and not t[7] and not t[6] then
      checkEdges := true;
      if (y = L-1) then
      if not t[8] and not t[3] and not t[4] and not t[2] and not t[1] then
      checkEdges := true;
    end else
    begin
      t1[1] := ((t[1] and t[2]) or t[2]) and not t[3] and not t[4] and not t[5] and not t[6] and not t[7] and not t[8];
      t1[2] := ((t[3] and t[2]) or t[2]) and not t[1] and not t[4] and not t[5] and not t[6] and not t[7] and not t[8];
      t1[3] := ((t[3] and t[4]) or t[4]) and not t[1] and not t[2] and not t[5] and not t[6] and not t[7] and not t[8];
      t1[4] := ((t[4] and t[5]) or t[4]) and not t[3] and not t[1] and not t[2] and not t[6] and not t[7] and not t[8];
      t1[5] := ((t[5] and t[6]) or t[6]) and not t[3] and not t[4] and not t[1] and not t[2] and not t[7] and not t[8];
      t1[6] := ((t[6] and t[7]) or t[6]) and not t[3] and not t[4] and not t[5] and not t[1] and not t[2] and not t[8];
      t1[7] := ((t[7] and t[8]) or t[8]) and not t[3] and not t[4] and not t[5] and not t[6] and not t[1] and not t[2];
      t1[8] := ((t[8] and t[1]) or t[8]) and not t[3] and not t[4] and not t[5] and not t[6] and not t[7] and not t[2];
      if t1[1] or t1[2] or t1[3] or t1[4] or t1[5] or t1[6] or t1[7] or t1[8] then
      checkEdges := true;
    end;
  end;
end;

function checkEnd(M: mas): boolean;
var i, j: integer;
begin
  checkEnd := true;
  for i := 2 to (L-1) do
    for j := 2 to (H-1) do
      if checkEdges(M,i,j) = true then
      checkEnd := false;
end;

function progres(M: mas): real;
var sk: real; i, j, k: integer;
begin
  k := 0;
  for i := 1 to L do
    for j := 1 to H do
      if M[i,j] = s then k := k + 1;
  progres := (k/(H * L))*100;
end;

procedure makeMaze(var M: mas);
var x, y: integer;
begin
  randomize;
  repeat
    x := random(H)+1; y := random(L)+1;
    if x = 1 then x := 2; if x = H then x := H - 1;
    if y = 1 then y := 2; if y = L then y := L - 1;
    if checkEdges(M, y, x) = true then
    M[y,x] := s;
    gotoxy(1,4);
    writeln((progres(M)*2):4:1);
    gotoxy(1,6); drawmaze(M);
  until ((checkEnd(M) = true) or keypressed);
end;

var
    Maze: mas; i, j: integer;
begin
  cursoroff;
  Write('Enter hight and lenght: '); read(H, L);
  getWalls(Maze);
  makeMaze(Maze); writeln('Maze have been compleated.'); readln;
  Maze[2,1] := ' '; Maze[L-1,H] := ' ';
  {for i := 1 to L do
  begin
    for j := 1 to H do
      if Maze[i,j] = s then
      write(#219) else
      begin
      write(' '); Maze[i,j] := '.';
      end;
    writeln;
  end; }
  saveMaze(Maze);
  readln; readln;
end.
